Drupal.behaviors.nodeReferenceAddNew = {
  attach: function (context, settings) {
    jQuery(document).bind("CToolsDetachBehaviors", function(context) { 
      if(settings.nodeReferenceAddNew != undefined){
        var node = settings.nodeReferenceAddNew.node.node;
        var field = node.title + ' [nid:' + node.nid + ']';
        var type = node.type;
        if(type){
          jQuery('#edit-field-project-coordinator-und-0-nid').val(field);
        } else {
          jQuery('#edit-field-' + type + '-und-0-nid').val(field);
        }
        
      }
    });
  }
};
